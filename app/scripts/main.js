jQuery(function ($) {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  new Swiper('.f-home-gallery__cards', {
    slidesPerView: 'auto',
    spaceBetween: 0,
    loop: true,
    freeMode: true,
    speed: 1,
    autoplay: {
      delay: 0,
      speed: 5000,
      disableOnInteraction: true,
    },
  });

  new Swiper('.f-home-clients__cards', {
    slidesPerView: 1,
    spaceBetween: 30,
    loop: true,
    navigation: {
      nextEl: '.f-home-clients__next',
      prevEl: '.f-home-clients__prev',
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1024: {
        slidesPerView: 4,
      },
      1200: {
        slidesPerView: 'auto',
      },
    },
  });

  new Swiper('.f-card__cards', {
    loop: true,
    pagination: {
      el: '.f-card .swiper-pagination',
    },
    navigation: {
      nextEl: '.f-card__next',
      prevEl: '.f-card__prev',
    },
  });

  $('.f-header__mob').on('click', function (e) {
    e.preventDefault();

    $('.f-header__nav').slideToggle('fast');
    $('.f-header__link').slideToggle('fast');
    $('.f-header__phone').slideToggle('fast');
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.f-modal').toggle();
  });

  $('.f-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'p-modal__centered') {
      $('.f-modal').hide();
    }
  });

  $('.f-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.f-modal').hide();
  });

  $('.f-view-card__icon').on('click', function (e) {
    e.preventDefault();

    if ($(this).next().is(':visible')) {
      $(this).next().toggle();
    } else {
      $('.f-view-card__info').each(function (e) {
        $(this).hide();
      });
      $(this).next().toggle();
    }
  });

  $('.open-more').on('click', function (e) {
    e.preventDefault();

    $('.f-more').toggle();
  });

  $('.f-more__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'p-more__centered') {
      $('.f-more').hide();
    }
  });

  $('.f-more__close').on('click', function (e) {
    e.preventDefault();
    $('.f-more').hide();
  });

  $('.f-home-seo__link').on('click', function (e) {
    e.preventDefault();

    if ($(this).hasClass('f-home-seo__link_active')) {
      $(this).html('Читать дальше');
      $(this).removeClass('f-home-seo__link_active');
      $('.f-home-seo__content').removeClass('f-home-seo__content_active');
    } else {
      $(this).html('Скрыть');
      $(this).addClass('f-home-seo__link_active');
      $('.f-home-seo__content').addClass('f-home-seo__content_active');
    }
  });

  $('.f-tabs__header li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('.f-tabs__header li').removeClass('current');
    $('.f-tabs__content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });

  $('.f-home-new__tabs li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('.f-home-new__tabs li').removeClass('current');

    $(this).addClass('current');
  });

  $('.f-switch__header li').click(function () {
    var switch_id = $(this).attr('data-switch');

    $('.f-switch__header li').removeClass('current');
    $('.f-switch__content').removeClass('current');

    $(this).addClass('current');
    $('#' + switch_id).addClass('current');
  });

  $(window).on('load resize', function () {
    if ($(window).width() > 1199) {
      gsap
        .timeline({
          scrollTrigger: {
            trigger: '.f-home-what',
            start: 'center center',
            end: 'center',
            scrub: true,
            pin: false,
          },
        })
        .from('.f-home-what__1', { rotation: 40, y: -150 })
        .from('.f-home-what__2', { rotation: 20, y: 150 })
        .from('.f-home-what__3', { rotation: -20, y: -250 })
        .from('.f-home-what__4', { rotation: 20, y: 50 })
        .from('.f-home-what__5, .f-home-what__5 > path', {
          y: -50,
          fill: 'none',
        })
        .from('.f-home-what__6, .f-home-what__6 > path', {
          y: 50,
          fill: 'none',
        })
        .from('.f-home-what__7, .f-home-what__7 > path', {
          y: -50,
          fill: 'none',
        })
        .from('.f-home-what__8, .f-home-what__8 > path', {
          y: 50,
          fill: 'none',
        })
        .from('.f-home-what__9, .f-home-what__9 > path', {
          y: -50,
          fill: 'none',
        })
        .from('.f-home-what__10, .f-home-what__10 > path', {
          y: 50,
          fill: 'none',
        })
        .from('.f-home-what__11, .f-home-what__11 > path', {
          y: -50,
          fill: 'none',
        })
        .from('.f-home-what__12', { y: -50, rotate: 45, opacity: 0 })
        .from('.f-home-what__13', { y: 50, rotate: -45, opacity: 0 })
        .from('.f-home-what__14', { y: -50, rotate: 45, opacity: 0 })
        .to('.f-home-what__1', { rotation: 40, y: -150 })
        .to('.f-home-what__2', { rotation: 20, y: 150 })
        .to('.f-home-what__3', { rotation: -20, y: -250 })
        .to('.f-home-what__4', { rotation: 20, y: 50 })
        .to('.f-home-what__5, .f-home-what__5 > path', {
          y: -50,
          fill: 'none',
        })
        .to('.f-home-what__6, .f-home-what__6 > path', {
          y: 50,
          fill: 'none',
        })
        .to('.f-home-what__7, .f-home-what__7 > path', {
          y: -50,
          fill: 'none',
        })
        .to('.f-home-what__8, .f-home-what__8 > path', {
          y: 50,
          fill: 'none',
        })
        .to('.f-home-what__9, .f-home-what__9 > path', {
          y: -50,
          fill: 'none',
        })
        .to('.f-home-what__10, .f-home-what__10 > path', {
          y: 50,
          fill: 'none',
        })
        .to('.f-home-what__11, .f-home-what__11 > path', {
          y: -50,
          fill: 'none',
        });
      /* .to('.f-home-what__12', { y: -50, rotate: 45, opacity: 0 })
        .to('.f-home-what__13', { y: 50, rotate: -45, opacity: 0 })
        .to('.f-home-what__14', { y: -50, rotate: 45, opacity: 0 }); */

      gsap
        .timeline({
          scrollTrigger: {
            trigger: '.f-home-features__title',
            start: 'center bottom',
            end: 'bottom',
            scrub: true,
            pin: false,
          },
        })
        .from('.f-home-features-card_1', { rotation: 20, scale: 0 })
        .from('.f-home-features-card_2', { rotation: -20, scale: 0 })
        .from('.f-home-features-card_3', { rotation: 20, scale: 0 })
        .from('.f-home-features-card_4', { rotation: -20, scale: 0 })
        .from('.f-home-features-card_5', { rotation: 20, scale: 0 })
        .from('.f-home-features-card_6', { rotation: -20, scale: 0 })
        .from('.f-home-features-card_7', { rotation: 20, scale: 0 });
    } else {
      $('.f-home-rent__link').detach().appendTo('.f-home-rent');
    }
  });
});
